import os

import Adafruit_DHT

from jacolib import assistant

# ==================================================================================================

file_path = os.path.dirname(os.path.realpath(__file__)) + "/"
assist: assistant.Assistant

sensor = None
gpio_pin = 0
offset_temperature = 0
offset_humidity = 0


# ==================================================================================================


def get_sensor_data():
    humidity, temperature = Adafruit_DHT.read_retry(sensor, gpio_pin)

    if humidity is None or temperature is None:
        # Sometimes reading doesn't work, try again in this case
        humidity, temperature = Adafruit_DHT.read_retry(sensor, gpio_pin)

    if humidity is None or temperature is None:
        return None, None

    temperature = temperature + offset_temperature
    temperature = round(temperature, 1)

    humidity = humidity + offset_humidity
    humidity = round(humidity)

    return humidity, temperature


# ==================================================================================================


def callback_read_temperature(message):
    """Callback to say temperature"""

    humidity, temperature = get_sensor_data()
    if humidity is not None and temperature is not None:
        result_sentence = assist.get_random_talk("temperature")
        result_sentence = result_sentence.format(temperature, humidity)
    else:
        result_sentence = assist.get_random_talk("reading_error")
    assist.publish_answer(result_sentence, message["satellite"])


# ==================================================================================================


def callback_read_humidity(message):
    """Callback to say humidity"""

    humidity, temperature = get_sensor_data()
    if humidity is not None and temperature is not None:
        result_sentence = assist.get_random_talk("humidity")
        result_sentence = result_sentence.format(humidity, temperature)
    else:
        result_sentence = assist.get_random_talk("reading_error")
    assist.publish_answer(result_sentence, message["satellite"])


# ==================================================================================================


def main():
    global assist, gpio_pin, sensor, offset_temperature, offset_humidity

    assist = assistant.Assistant(repo_path=file_path)

    gpio_pin = int(assist.get_config()["user"]["gpio_pin"])
    offset_temperature = float(assist.get_config()["user"]["offset_temperature"])
    offset_humidity = float(assist.get_config()["user"]["offset_humidity"])

    dht_sensor = int(assist.get_config()["user"]["dht_sensor"])
    if dht_sensor == 22:
        sensor = Adafruit_DHT.DHT22
    elif dht_sensor == 11:
        sensor = Adafruit_DHT.DHT11
    else:
        raise NotImplementedError

    # Print current values for debugging
    print("Current sensor values:", get_sensor_data())

    assist.add_topic_callback("read_temperature", callback_read_temperature)
    assist.add_topic_callback("read_humidity", callback_read_humidity)
    assist.run()


# ==================================================================================================

if __name__ == "__main__":
    main()
